const express = require('express');
const { Client } = require('pg');
const cors = require('cors');

const app = express();
const port = 3000;

// Enable CORS
app.use(cors());

// Parse JSON bodies
app.use(express.json());

// Replace with your database credentials
const client = new Client({
  user: 'postgres',
  host: 'database-2-instance-1.cvyu6euk2fr8.eu-west-1.rds.amazonaws.com',
  database: 'postgres',
  password: 'HelloWorld#27',
  port: 5432,
});

client.connect()
  .then(() => {
    console.log('Database connected successfully');
  })
  .catch(err => {
    console.error('Error connecting to database:', err);
  });

// SELECT
async function getDataFromTable(tableName, whereClause = '', res) {
  try {
    let query = `SELECT * FROM ${tableName}`;
    if (whereClause) {
      query += ` WHERE ${whereClause}`;
    }
    const result = await client.query(query);
    const data = result.rows;
    res.json(data);
    console.log(data);
  } catch (err) {
    console.error('Error executing query', err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// UPDATE
async function updateDataInTable(tableName, updateValues, whereClause = '', res) {
  try {
    let query = `UPDATE ${tableName} SET ${updateValues}`;
    if (whereClause) {
      query += ` WHERE ${whereClause}`;
    }    
    await client.query(query);
    res.status(200).json({ message: 'Data updated successfully', data: updateValues });
  } catch (err) {
    console.error('Error updating data:', err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// Route handler
app.all('/api/data/:tableName', async (req, res) => {
  const { tableName } = req.params;
  if (req.method === 'GET') {
    const { whereClause } = req.query;
    await getDataFromTable(tableName, whereClause, res);
    console.log('here is fetch..........');
  } else if (req.method === 'PUT') {
    const { updateValues, whereClause } = req.body;
    await updateDataInTable(tableName, updateValues, whereClause, res);
  } else {
    res.status(405).json({ error: 'Method Not Allowed' }); 
  }
});

app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});
