import React, { Component, useState } from "react";
import { StyleSheet, Modal, View, Pressable, Dimensions } from "react-native";
import { HiraginoKakuText } from "../StyledText";
import { HeadingXxxSmallBold } from "../../styles/typography";
import { colors } from "../../styles/color";
import { CustomButton } from "./Button";
import { Ionicons } from "@expo/vector-icons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

interface ModalProps {
  text?: string;
  firstButtonText: string;
  firstButtonType: string;
  secondButtonText: string;
  secondButtonType: string;
  leftButtonText: string;
  leftButtonType: string;
  firstButtonDisable: boolean;
  secondButtonDisable: boolean;
  firstButtonVisible: boolean;
  secondButtonVisible: boolean;
  leftButtonVisible: boolean;
  closeButtonVisible: boolean;
  firstButtonWidth: number;
  secondButtonWidth: number;
  leftButtonWidth: number;
  firstBtnTextWidth: number;
  secondBtnTextWidth: number;
  leftBtnTextWidth: number;
  onFirstButtonPress: () => void;
  onSecondButtonPress: () => void;
  onLeftButtonPress: () => void;
  handleSecondButtonEnable: () => void;
  children?: React.ReactNode;
  toggleModal: () => void;
}

class ModalComponent extends Component<ModalProps> {
  static defaultProps: Partial<ModalProps> = {
    text: "",
    firstButtonText: "",
    secondButtonText: "",
    leftButtonText: "",
    firstButtonDisable: false,
    secondButtonDisable: false,
    firstButtonType: "",
    secondButtonType: "",
    leftButtonType: "",
    firstButtonVisible: true,
    secondButtonVisible: true,
    leftButtonVisible: false,
    closeButtonVisible: true,
    firstButtonWidth: 120,
    secondButtonWidth: 120,
    leftButtonWidth: 120,
    firstBtnTextWidth: 80,
    secondBtnTextWidth: 80,
    leftBtnTextWidth: 80,
  };

  state = {
    modalVisible: true,
  };

  toggleModalVisibility = () => {
    this.setState({ modalVisible: !this.state.modalVisible });
    this.props.toggleModal();
  };

  render() {
    const { modalVisible } = this.state;
    return (
      <Modal transparent={true} visible={modalVisible} focusable={false}>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={styles.modalBackground}
          scrollEnabled={false}
        >
          <View style={[styles.modalContainer]}>
            <View style={styles.modalHeader}>
              <HiraginoKakuText style={styles.headerText}>
                {this.props.text}
              </HiraginoKakuText>
              {this.props.closeButtonVisible && (
                <View style={styles.headerButtonContainer}>
                  <Pressable
                    style={styles.closeButton}
                    onPress={this.toggleModalVisibility}
                  >
                    <Ionicons
                      name="close-sharp"
                      size={24}
                      style={styles.closeicon}
                      color="black"
                    />
                  </Pressable>
                </View>
              )}
            </View>
            {this.props.children != null && (
              <View style={styles.modalBody}>{this.props.children}</View>
            )}
            <View style={styles.modalFooter}>
              <View style={styles.leftButtonContainer}>
                {this.props.leftButtonVisible && (
                  <CustomButton
                    style={styles.modalButton}
                    text={this.props.leftButtonText}
                    type={
                      this.props.leftButtonType != ""
                        ? this.props.leftButtonType
                        : "ButtonMGray"
                    }
                    onPress={this.props.onLeftButtonPress}
                    buttonWidth={this.props.leftButtonWidth}
                    textWidth={this.props.leftBtnTextWidth}
                  />
                )}
              </View>
              <View style={styles.buttonContainer}>
                {this.props.firstButtonVisible && (
                  <CustomButton
                    style={styles.modalButton}
                    text={this.props.firstButtonText}
                    type={
                      this.props.firstButtonDisable
                        ? "ButtonMDisable"
                        : this.props.firstButtonType != ""
                        ? this.props.firstButtonType
                        : "ButtonMGray"
                    }
                    onPress={this.props.onFirstButtonPress}
                    buttonWidth={this.props.firstButtonWidth}
                    textWidth={this.props.firstBtnTextWidth}
                  />
                )}
                {this.props.secondButtonVisible && (
                  <CustomButton
                    style={styles.modalButton}
                    text={this.props.secondButtonText}
                    type={
                      this.props.secondButtonDisable
                        ? "ButtonMDisable"
                        : this.props.secondButtonType != ""
                        ? this.props.secondButtonType
                        : "ButtonMPrimary"
                    }
                    onPress={this.props.onSecondButtonPress}
                    buttonWidth={this.props.secondButtonWidth}
                    textWidth={this.props.secondBtnTextWidth}
                  />
                )}
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Modal>
    );
  }
}
export default ModalComponent;

export const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    backgroundColor: colors.transparentColor,
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    width: 545,
    backgroundColor: colors.secondary,
    borderRadius: 12,
    overflow: "hidden",
    alignItems: "center",
  },
  modalHeader: {
    width: "100%",
    minHeight: 59,
    justifyContent: "space-between",
    paddingHorizontal: 24,
    paddingVertical: 16,
    backgroundColor: colors.secondary,
    borderBottomWidth: 1,
    borderColor: colors.gray,
    flexDirection: "row",
  },
  headerText: {
    width: 441,
    minHeight: 27,
    fontSize: HeadingXxxSmallBold.size,
    lineHeight: HeadingXxxSmallBold.lineHeight,
    verticalAlign: "middle",
    color: colors.textColor,
  },
  headerButtonContainer: {
    width: 56,
    height: "auto",
    gap: 8,
    backgroundColor: colors.secondary,
    verticalAlign: "middle",
  },
  closeButton: {
    width: 24,
    height: 24,
    alignSelf: "flex-end",
    alignItems: "center",
  },
  closeicon: {
    height: 24,
  },
  modalBody: {
    width: "100%",
    padding: 24,
    backgroundColor: colors.secondary,
    gap: 16,
  },
  modalFooter: {
    width: "100%",
    height: 76,
    justifyContent: "space-between",
    paddingHorizontal: 24,
    paddingVertical: 16,
    backgroundColor: colors.secondary,
    borderTopWidth: 1,
    borderColor: colors.gray,
    flexDirection: "row",
  },
  leftButtonContainer: {
    height: 44,
  },
  buttonContainer: {
    width: 240,
    height: 44,
    gap: 16,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  modalButton: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    paddingHorizontal: 20,
    paddingVertical: 10,
    gap: 8,
    height: 44,
  },
});
