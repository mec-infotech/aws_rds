export const fetchData = async (tableName: any, whereClause = '') => {
  try {
  
    let url = `http://192.168.1.5:3000/api/data/${tableName}`;
    if (whereClause) {
      url += `?whereClause=${encodeURIComponent(whereClause)}`;
    }
    
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    
    const jsonData = await response.json();
    return jsonData;
  } catch (error) {
    console.error('Error fetching data:', error);
  }
};

export const updateData = async (tableName: string, updates: Record<string, any>, whereClause: string = '') => {
  try {
    const updateArray = Object.entries(updates).map(([key, value]) => `${key} = '${value}'`);
    const updateValues = updateArray.join(', ');

    const url = `http://192.168.1.5:3000/api/data/${encodeURIComponent(tableName)}`;

    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        updateValues,
        whereClause
      })
    });

    if (!response.ok) {
      throw new Error('Failed to update data');
    }

    const jsonData = await response.json();
    return jsonData;
  } catch (error) {
    console.error('Error updating data:', error);
    throw error; // Rethrow the error to propagate it to the caller
  }
};
