import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  TextInput,
  Pressable,
  StatusBar,
  SafeAreaView,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Ionicons } from "@expo/vector-icons";
import styles from "./LoginStyles";
import { colors } from "../../styles/color";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { CustomButton } from "../../components/basics/Button";
import { Header } from "../../components/basics/header";
import { realtimeDB } from "../../config/firebaseConfig";
import { get, ref } from "firebase/database";
import { NavigationProp } from "@react-navigation/native";
import * as crypto from "crypto-js";
import { fetchData, updateData } from "../../config/commonDBfuns";

type Props = {
  navigation: NavigationProp<any, any>;
};

interface UserData {
  id: number;
  name: string;
  age: number;
}

export const Login = ({ navigation }: Props) => {
  const [userid, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [inputNotEmpty, setInputNotEmpty] = useState(false);

  // AWS  
  const [data, setData] = useState<UserData[]>([]);
  const [johnData, setJohnData] = useState([]);
  useEffect(() => {
    // SELECT ALL
    fetchData('user_table')
      .then((userData) => {
        setData(userData);
        console.log(userData);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });

    // SELECT With Condition
    fetchData('user_table', "age > 30 AND name = 'John'")
      .then((JohnData) => {
        setJohnData(JohnData);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });

    // UPDATE
    updateData('user_table', { name: 'John', age: 99 }, 'id = 1')
      .then(response => {
        console.log('Data updated successfully:', response);
      })
      .catch(error => {
        console.error('Error updating data:', error);
      });
  }, []);

  const checkLogin = () => {
    const userRef = ref(realtimeDB, "baseMember");
    // 'baseMember'ノードからデータをフェッチする
    get(userRef)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          //console.log("ユーザーデータ: ", userData);

          // ログインをチェック
          let loggedIn = false;
          for (const userId in userData) {
            if (userData.hasOwnProperty(userId)) {
              const user = userData[userId];
              let hashpassword = crypto.SHA256(password).toString(crypto.enc.Hex);
              if (user.id === userid && user.password === hashpassword) {
                loggedIn = true;
                setErrorMessage("");
                navigation.navigate("EventList", { userId: userid });
                console.log("ログイン成功");
                break;
              }
            }
          }
          if (!loggedIn) {
            setErrorMessage("IDまたはパスワードが正しくありません");
            console.log("IDまたはパスワードが正しくありません");
          }
        } else {
          console.log("ユーザーデータが見つかりません");
        }
      })
      .catch((error) => {
        console.error("ユーザーデータの取得に失敗しました", error);
      });
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleInputChange = (text: string, isPassword: boolean = false) => {
    if (isPassword) {
      setPassword(text);
      setInputNotEmpty(userid.trim().length > 0 && text.trim().length > 0);
    } else {
      setUserId(text);
      setInputNotEmpty(text.trim().length > 0 && password.trim().length > 0);
    }
  };

  const renderEyeIcon = () => {
    return (
      <Pressable style={styles.eyeIconContainer} hitSlop={18}>
        <Ionicons
          name={!showPassword ? "eye-off" : "eye"}
          size={24}
          color={colors.secondary}
          style={styles.eyeIcon}
          onPress={togglePasswordVisibility}
        />
      </Pressable>
    );
  };

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header
          middleTitleName="LGaP受付 ポータルアプリ"
          buttonName=""
          hasButton={false}
        />

        <View style={styles.bodyContainer}>

          {/* AWS */}
          <View >
            {/* SELECT ALL */}
            <View style={{ flexDirection: "row" }}>
              <Text>SELECT ALL:   </Text>
              {data.map(item => (
                <Text key={item.id} style={{ fontWeight: "900" }}>
                  {item.name} , {item.age} ||
                </Text>
              ))}
            </View>

            {/* SELECT with Conditon */}
            <View style={{ flexDirection: "row" }}>
              <Text>SELECT With Condition Age {">"} 30 and Name = John:   </Text>
              {johnData.map((item: any) => (
                <Text key={item.id} style={{ fontWeight: "900" }}>
                  {item.name} , {item.age} ||
                </Text>
              ))}
            </View>
          </View>

          <HiraginoKakuText style={styles.loginText}>ログイン
            {/* {data.map(item => (
              <Text key={item.id}>{item.name} , {item.age} || </Text>
            ))} */}
          </HiraginoKakuText>

          <View style={styles.infoBox}>
            <View style={styles.inputContainer}>
              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>ID</HiraginoKakuText>
                <View style={[styles.passwordBox]}>
                  <TextInput
                    placeholder="ID"
                    placeholderTextColor={colors.placeholderTextColor}
                    onChangeText={(text) => handleInputChange(text, false)}
                    value={userid}
                    style={[styles.input]}
                  />
                </View>
              </View>

              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>
                  パスワード
                </HiraginoKakuText>

                <View style={[styles.passwordBox]}>
                  <TextInput
                    style={[styles.input]}
                    secureTextEntry={!showPassword}
                    placeholder="パスワード"
                    placeholderTextColor={colors.placeholderTextColor}
                    value={password}
                    onChangeText={(text) => handleInputChange(text, true)}
                    passwordRules=""
                  />

                  {renderEyeIcon()}
                </View>
                {errorMessage !== "" && (
                  <View style={styles.messageContainer}>
                    <HiraginoKakuText style={styles.errorMessage} normal>
                      {errorMessage}
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
            </View>
            <CustomButton
              text="ログイン"
              onPress={() => checkLogin()}
              style={styles.buttonLogin}
              type={inputNotEmpty ? "ButtonMPrimary" : "ButtonMDisable"}
            />
          </View>
        </View>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};
