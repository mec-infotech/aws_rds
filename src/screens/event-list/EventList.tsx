import React, { useState } from "react";
import {
  StatusBar,
  SafeAreaView,
  TextInput,
  Pressable,
  Modal,
  FlatList,
} from "react-native";
import styles from "./EventListStyle";
import { Header } from "../../components/basics/header";
import { View } from "../../components/Themed";
import { CustomButton } from "../../components/basics/Button";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { StatusChangeDialog } from "../status-change-dialog/StatusChangeDialog";
import { EventDeleteDialog } from "../event-delete-dialog/EventDeleteDialog";
import { Logout } from "../logout/Logout";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventList = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId } = route.params as { userId: string };
  const [showInputs, setShowInputs] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [isStatusChangeModalVisible, setIsStatusChangeModalVisible] =
    useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [isLogOutModalVisible, setIsLogOutModalVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState("最終更新日が新しい");
  useState(false);
  const defaultOnPress = () => {};
  const handleCalendarPress = () => {};
  const toggleInputs = () => {
    setShowInputs(!showInputs);
  };

  //delete dialog
  const openEventDeleteModal = () => {
    setIsDeleteModalVisible(true);
  };
  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventList", { userId: userId, eventId: 0 });
  };
  const handleEventDetail = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: 1,
      pgType: "EventList",
    });
  };

  //logout Dialog
  const handleLogOut = () => {
    setIsLogOutModalVisible(true);
  };
  const handleLogoutCancelButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("EventList", { userId: userId });
  };
  const handleLogoutButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("Login");
  };
  const handleCreateEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    navigation.navigate("EventCreate", { userId: userId });
  };
  const handleEditEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    navigation.navigate("EventEdit", { userId: userId, eventId: 1 });
  };
  const handleReproductionEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    navigation.navigate("EventCreate", { userId: userId });
  };
  const handleDeleteEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    openEventDeleteModal();
  };

  //status change dialog
  const [selectedStatusName, setSelectedStatusName] = useState("");
  const handleStatusChangeEvent = (status: string) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setSelectedStatusName(status);
    setIsStatusChangeModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsStatusChangeModalVisible(false);
    navigation.navigate("EventList", { userId: userId, eventId: 0 });
  };

  const handleStatusChangePreparationEvent = (status: string) => {
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: 1,
      statusName: status,
    });
  };

  // Dropdown
  const dropdownData = [
    { label: "最終更新日が新しい", value: "newest" },
    { label: "最終更新日が古い", value: "oldest" },
  ];

  // Edit Dropdown
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSecondDropdown, setShowSecondDropdown] = useState(false);
  const [selectedDropDownValue, setSelectedDropDownValue] = useState("");
  const handleDropDown = () => {
    setShowDropdown(!showDropdown);
    setShowSecondDropdown(false);
  };

  // Pagination
  const DATA = [
    {
      id: 1,
      eventName: "出茂マラソン大会2024",
      venue: "会場1",
      eventPeriod: "",
      numberofPeople: "0",
    },
    {
      id: 2,
      eventName: "ピザ作り",
      venue: "会場1",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "0",
    },
    {
      id: 3,
      eventName: "福祉・保育のおしごと相談",
      venue: "センター",
      eventPeriod: "",
      numberofPeople: "100",
    },
    {
      id: 4,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "",
      numberofPeople: "1",
    },
    {
      id: 5,
      eventName: "空き家対策セミナー",
      venue: "会場1",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "86",
    },
    {
      id: 6,
      eventName: "出茂マラソン大会2023",
      venue: "A会場",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "60",
    },
    {
      id: 7,
      eventName: "つくってみよう",
      venue: "福祉会館",
      eventPeriod: "",
      numberofPeople: "120",
    },
    {
      id: 8,
      eventName: "乳幼児健康相談",
      venue: "A会場、B会場、C会場、D会場",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "60",
    },
  ];

  const ITEMS_PER_PAGE = 8;
  const [page, setPage] = useState(1);

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.receptionStatusHeader}>
        <HiraginoKakuText style={styles.receptionStatusText}>
          受付状況
        </HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View style={[styles.eachHeadingContainer, styles.eventHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント名
        </HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.venueHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>会場</HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.eventDateText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント期間
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonHeader}>
        <HiraginoKakuText style={[styles.acceptedPersonHeaderText]}>
          受付した人
        </HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View></View>
    </View>
  );

  const renderTableItem = ({ item }: { item: any }) => (
    <Pressable style={styles.row} onPress={handleEventDetail}>
      <View style={[styles.cell, styles.receptionStatusBodyText]}>
        {item.id === 3 || item.id === 4 || item.id === 5 ? (
          <View style={styles.receptionStatusPrimaryLabel}>
            <HiraginoKakuText style={styles.receptionStatusPrimaryLabelText}>
              受付可能
            </HiraginoKakuText>
          </View>
        ) : item.id === 6 || item.id === 7 || item.id === 8 ? (
          <View style={styles.receptionStatusDefaultLabel}>
            <HiraginoKakuText style={styles.receptionStatusDefaultLabelText}>
              受付終了
            </HiraginoKakuText>
          </View>
        ) : (
          <View style={styles.receptionStatusLabel}>
            <HiraginoKakuText style={[styles.receptionStatusLabelText]}>
              準備中
            </HiraginoKakuText>
          </View>
        )}
      </View>
      <View style={[styles.cell, styles.eventTableBodyContainer]}>
        <HiraginoKakuText style={styles.eventTableBodyText} numberOfLines={1}>
          {item.eventName}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.venueBodyContainer]}>
        <HiraginoKakuText style={styles.venueBodyText} numberOfLines={1} normal>
          {item.venue}
        </HiraginoKakuText>
      </View>
      <View style={styles.eventDateBodyContainer}>
        <HiraginoKakuText style={styles.eventDateBodyText} normal>
          {item.eventPeriod}
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonBodyContainer}>
        <HiraginoKakuText style={styles.acceptedPersonBodyText}>
          {item.numberofPeople}
        </HiraginoKakuText>
      </View>
      <Pressable style={styles.cell} onPress={handleDropDown}>
        <View style={styles.threeDotsChildContainer}>
          <MaterialIcons
            name="more-horiz"
            size={16}
            color={colors.greyTextColor}
          />
        </View>
      </Pressable>
    </Pressable>
  );

  const getPageData = () => {
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return DATA.slice(startIndex, endIndex);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        middleTitleName="LGaP受付 ポータルアプリ"
        buttonName="ログアウト"
        onPress={handleLogOut}
      ></Header>

      <FlatList
        data={[{ key: "content" }]}
        renderItem={() => (
          <View style={styles.bodyContainer}>
            <View style={styles.bodyTitleContainer}>
              <HiraginoKakuText style={styles.bodyText}>
                イベント
              </HiraginoKakuText>
              <CustomButton
                text="イベント作成"
                onPress={handleCreateEvent}
                style={styles.bodyTitleButton}
                type="ButtonMPrimary"
              />
            </View>
            <View style={styles.mainInfoContainer}>
              <View style={[styles.firstChildContainer]}>
                <View style={styles.infoContainer}>
                  <View style={styles.parentInputContainer}>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          イベント名
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.inputContainer}>
                        <TextInput
                          style={styles.input}
                          placeholder="イベントタイトル"
                          placeholderTextColor={colors.placeholderTextColor}
                        />
                      </View>
                    </View>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          受付状況
                        </HiraginoKakuText>
                      </View>
                      <Pressable
                        style={styles.multiSelectContainer}
                        onPress={defaultOnPress}
                      >
                        <Entypo
                          name="chevron-down"
                          size={20}
                          color={colors.activeCarouselColor}
                          style={styles.multiDropdownIconStyle}
                        />
                      </Pressable>
                    </View>
                  </View>
                  <>
                    <Pressable
                      style={styles.dropdownContainer}
                      onPress={toggleInputs}
                    >
                      <HiraginoKakuText style={styles.dropdownText}>
                        詳細検索
                      </HiraginoKakuText>
                      <Entypo
                        name={showInputs ? "chevron-up" : "chevron-down"}
                        size={24}
                        color={colors.primary}
                        style={styles.iconStyle}
                      />
                    </Pressable>
                    {showInputs && (
                      <View style={styles.hiddenContainer}>
                        <View style={styles.parentInputContainer}>
                          <View style={styles.childInputContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                会場
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.inputContainer}>
                              <TextInput
                                style={styles.input}
                                placeholder="会場名"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                              />
                            </View>
                          </View>
                        </View>
                        <View style={[styles.mainDateContainer]}>
                          <View style={styles.parentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                終了日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                />
                                <Pressable
                                  style={styles.calendarIconContainer}
                                  onPress={handleCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                />
                                <Pressable
                                  style={styles.calendarIconContainer}
                                  onPress={handleCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                              </View>
                            </View>
                          </View>
                          <View style={styles.parentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                開始日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                />
                                <Pressable
                                  style={styles.calendarIconContainer}
                                  onPress={handleCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                />
                                <Pressable
                                  style={styles.calendarIconContainer}
                                  onPress={handleCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    )}
                  </>
                </View>
                <View style={styles.buttonContainer}>
                  <CustomButton
                    text="クリア"
                    onPress={defaultOnPress}
                    style={styles.grayMButton}
                    type="ButtonMediumGray"
                    textWidth={48}
                  />
                  <CustomButton
                    text="検索"
                    onPress={defaultOnPress}
                    style={styles.PrimaryMButton}
                    type="ButtonMPrimary"
                  />
                </View>
              </View>
              <View style={styles.parentPaginationContainer}>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      1-50 / 9,999 件中
                    </HiraginoKakuText>
                  </View>
                  <Pressable
                    style={styles.sortingContainer}
                    onPress={defaultOnPress}
                  >
                    <MaterialCommunityIcons
                      name="swap-vertical"
                      size={20}
                      color={colors.activeCarouselColor}
                    />
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {selectedOption}
                    </HiraginoKakuText>
                    <Entypo
                      name="chevron-down"
                      size={20}
                      color={colors.greyTextColor}
                      style={styles.dropdownIconStyle}
                    />
                  </Pressable>

                  {/* Dropdown Modal */}
                  <Modal
                    visible={isDropdownVisible}
                    transparent={true}
                    animationType="fade"
                    onRequestClose={() => setIsDropdownVisible(false)}
                  >
                    <View style={styles.modalContainer}>
                      <FlatList
                        data={dropdownData}
                        keyExtractor={(item) => item.value}
                        renderItem={({ item }) => (
                          <Pressable
                            style={styles.dropdownItem}
                            onPress={defaultOnPress}
                          >
                            <HiraginoKakuText
                              style={styles.paginationCount}
                              normal
                            >
                              {item.label}
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                      />
                    </View>
                  </Modal>
                </View>

                <View style={styles.tableContainer}>
                  <FlatList
                    ListHeaderComponent={renderHeader}
                    data={getPageData()}
                    renderItem={renderTableItem}
                    keyExtractor={(item) => item.id.toString()}
                  />
                  {showDropdown && (
                    <View style={styles.dropdown}>
                      <Pressable
                        onPress={() =>
                          setShowSecondDropdown(!showSecondDropdown)
                        }
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付状況を変更
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleEditEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          編集
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleReproductionEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          複製
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleDeleteEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.deleteBodyText]}
                        >
                          削除
                        </HiraginoKakuText>
                      </Pressable>
                    </View>
                  )}
                  {showSecondDropdown && (
                    <View style={styles.secondDropdown}>
                      <Pressable onPress={defaultOnPress}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          準備中
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("受付可能")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付可能
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("受付終了")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付終了
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("アーカイブ")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          アーカイブ
                        </HiraginoKakuText>
                      </Pressable>
                    </View>
                  )}
                </View>
                <View style={styles.onChangePageContainer}>
                  <View style={styles.previousButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.skipBackButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="skip-back"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.chevronLeftButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="chevron-left"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                  <View style={styles.pageNumberContainer}>
                    <CustomButton
                      text="1"
                      onPress={defaultOnPress}
                      style={styles.numOneButton}
                      type="ButtonSPrimary"
                    />
                    <CustomButton
                      text="2"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <CustomButton
                      text="3"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <HiraginoKakuText style={styles.threeDots} normal>
                      …
                    </HiraginoKakuText>
                    <CustomButton
                      text="50"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                  </View>
                  <View style={styles.nextButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="chevron-right"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="skip-forward"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        )}
      />

      {isStatusChangeModalVisible && (
        <StatusChangeDialog
          statusName={selectedStatusName}
          onCancelButtonPress={handleCancelButton}
          onChangeButtonPress={handleCancelButton}
        />
      )}
      {isDeleteModalVisible && (
        <EventDeleteDialog
          onCancelButtonPress={handleDeleteCancelButton}
          onDeleteButtonPress={handleDeleteCancelButton}
        />
      )}
      {isLogOutModalVisible && (
        <Logout
          onCancelButtonPress={handleLogoutCancelButton}
          onLogoutButtonPress={handleLogoutButton}
        />
      )}
    </SafeAreaView>
  );
};
