// EventCreate.tsx
import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  Pressable,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import styles from "./EventCreateStyles";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { HiraginoKakuText } from "../../components/StyledText";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { EventCreatCancelDialog } from "../event-create-cancel-dialog/EventCreateCancelDialog";
type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventCreate = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId } = route.params as {
    userId: string;
    eventId: number;
  };

  const [inputBoxes, setInputBoxes] = useState([""]);
  const [selectedButton, setSelectedButton] = useState<string | null>(null);
  const [isCreateCancelModalVisible, setIsCreateCancelModalVisible] =
    useState(false);
  const addInputBox = () => {
    setInputBoxes([...inputBoxes, ""]);
  };

  const removeInputBox = (removedIndex: number) => {
    setInputBoxes(inputBoxes.filter((box, index) => index !== removedIndex));
  };

  const handleButtonPressMain = () => {
    if (selectedButton == "preparation") {
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: 1,
        pgType: "Create",
      });
    } else if (selectedButton == "acceptance") {
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: 1,
        pgType: "Create",
      });
    } else {
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: 1,
        pgType: "Create",
      });
    }
  };
  const handleButtonPress = (selectedButton: string) => {
    if (selectedButton === "create") {
      setSelectedButton(null);
    } else {
      setSelectedButton(selectedButton);
    }
  };

  const handleClose = () => {
    setIsCreateCancelModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsCreateCancelModalVisible(false);
    navigation.navigate("EventCreate", { userId: userId });
  };
  const handleFinishButton = () => {
    setIsCreateCancelModalVisible(false);
    navigation.navigate("EventList", { userId: userId });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="イベント作成" buttonName="">
          <CustomButton
            style={styles.btnCloseContainer}
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleClose}
          />
        </Header>
        <ScrollView style={styles.scrollViewContent}>
          <View style={styles.container}>
            <View style={styles.bodyContainer}>
              <View style={styles.eventTitleContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    イベント名
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View
                      style={[styles.statusBox, styles.eventTitleStatusBox]}
                    >
                      <HiraginoKakuText
                        style={[
                          styles.statusLabelText,
                          styles.eventRedStatusLblText,
                        ]}
                      >
                        必須
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <View>
                  <TextInput
                    placeholder="イベントタイトル"
                    placeholderTextColor={colors.placeholderTextColor}
                    style={styles.eventInputBox}
                  />
                </View>
              </View>
              <View style={styles.eventTimeContainer}>
                <View style={styles.eventTimeLblGpContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベント期間
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View style={styles.statusBox}>
                        <HiraginoKakuText style={styles.statusLabelText}>
                          任意
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <HiraginoKakuText normal style={styles.eventLabel}>
                    開始日と終了日を入力してください
                  </HiraginoKakuText>
                </View>
                <View style={styles.dateTimeSelectContainer}>
                  <View style={styles.DateContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      開始日
                    </HiraginoKakuText>
                    <View style={styles.datePickerContainer}>
                      <TextInput
                        style={styles.dateInput}
                        placeholder="日付を選択"
                        placeholderTextColor={colors.placeholderTextColor}
                      />
                      <Pressable style={styles.calendarIconContainer}>
                        <MaterialIcons
                          name="calendar-today"
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.calendarIcon}
                        />
                      </Pressable>
                    </View>
                  </View>
                  <View style={styles.waveDash}>
                    <Text style={styles.LabelLargeBold}> ~ </Text>
                  </View>
                  <View style={styles.DateContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      終了日
                    </HiraginoKakuText>
                    <View style={styles.datePickerContainer}>
                      <TextInput
                        style={styles.dateInput}
                        placeholder="日付を選択"
                        placeholderTextColor={colors.placeholderTextColor}
                      />
                      <Pressable style={styles.calendarIconContainer}>
                        <MaterialIcons
                          name="calendar-today"
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.calendarIcon}
                        />
                      </Pressable>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.eventVenueContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    会場
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View style={styles.statusBox}>
                      <HiraginoKakuText style={styles.statusLabelText}>
                        任意
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <View style={styles.venueInputBoxesContainer}>
                  {inputBoxes.map((box, index) => (
                    <View key={index} style={styles.venueInputBox}>
                      <TextInput
                        placeholder="会場名"
                        placeholderTextColor={colors.placeholderTextColor}
                        style={[styles.eventInputBox, styles.venueInput]}
                      />
                      {inputBoxes.length > 1 && (
                        <TouchableOpacity
                          onPress={() => removeInputBox(index)}
                          style={styles.venueCrossIconContainer}
                        >
                          <MaterialIcons
                            name="close"
                            size={22}
                            color="#515867"
                          />
                        </TouchableOpacity>
                      )}
                    </View>
                  ))}
                  <TouchableOpacity
                    style={styles.venueBtnContainer}
                    onPress={addInputBox}
                  >
                    <MaterialIcons name="add" size={22} color="#515867" />
                    <HiraginoKakuText
                      style={[
                        styles.bodyText,
                        styles.LabelLargeBold,
                        styles.venueBtnText,
                      ]}
                    >
                      会場を追加
                    </HiraginoKakuText>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.horizontalLine} />
              <View style={styles.eventReceptionContainer}>
                <View style={styles.eventHeadingLblContainer}>
                  <HiraginoKakuText
                    style={[styles.bodyText, styles.LabelLargeBold]}
                  >
                    イベントの受付状況
                  </HiraginoKakuText>
                  <View style={styles.statusLabelContainer}>
                    <View
                      style={[styles.statusBox, styles.eventTitleStatusBox]}
                    >
                      <HiraginoKakuText
                        style={[
                          styles.statusLabelText,
                          styles.eventRedStatusLblText,
                        ]}
                      >
                        必須
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
                <HiraginoKakuText normal style={styles.eventLabel}>
                  受付可能にすると受付アプリに表示されます
                </HiraginoKakuText>
                <View style={styles.eventReceptionBtnContainer}>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      selectedButton === "preparation" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("preparation")}
                  >
                    <HiraginoKakuText
                      style={[styles.receptionText, styles.LabelLargeBold]}
                    >
                      準備中
                    </HiraginoKakuText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      styles.receptionBtn,
                      styles.acceptBtn,
                      selectedButton === "acceptance" && styles.btnSelected,
                    ]}
                    onPress={() => handleButtonPress("acceptance")}
                  >
                    <HiraginoKakuText
                      style={[styles.buttonText, styles.LabelLargeBold]}
                    >
                      受付可能
                    </HiraginoKakuText>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.createBtnContainer}>
              <TouchableOpacity
                style={[
                  styles.createBtn,
                  selectedButton === "create" && styles.btnSelected,
                ]}
                onPress={handleButtonPressMain}
              >
                <HiraginoKakuText
                  style={[styles.createBtnText, styles.LabelLargeBold]}
                >
                  作成する
                </HiraginoKakuText>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        {isCreateCancelModalVisible && (
          <EventCreatCancelDialog
            onCancelButtonPress={handleCancelButton}
            onFinishButtonPress={handleFinishButton}
          ></EventCreatCancelDialog>
        )}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
